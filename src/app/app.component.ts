import {Component, ViewChild} from "@angular/core";
import {Nav, Platform} from "ionic-angular";
import {StatusBar, Splashscreen} from "ionic-native";
import {DictionaryComponent} from "./dictionary/dictionary.component";
import {ConfigComponent} from "./config/config.component";

@Component({
    templateUrl: 'app.component.html',
})
export class AppComponent {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = DictionaryComponent;

    pages: Array<{title: string, component: any}>;

    constructor(public platform: Platform) {
        this.initializeApp();

        // used for an example of ngFor and navigation
        this.pages = [
            {title: 'Картотека', component: DictionaryComponent},
            {title: 'Настройки', component: ConfigComponent}
        ];
    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            StatusBar.styleDefault();
            Splashscreen.hide();
        });
    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }
}
