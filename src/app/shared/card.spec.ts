/**
 * Created by vya on 12.01.2017.
 */
import {inject, TestBed} from "@angular/core/testing";
import {Dictionary} from "./dictionary.model";
import {AppModule} from "../app.module";
import {PracticeComponent} from "../practice/practice.component";
import {DictionaryComponent} from "../dictionary/dictionary.component";
import {CardComponent} from "../card/card.component";
import {AppComponent} from "../app.component";
import {ConfigComponent} from "../config/config.component";
import {CardDataService} from "./card.service";
import {DictionaryDataService} from "./dictionary.service";
import {Card} from "./card.model";

describe('Сервис карточки', () => {
    let dictionaryDataService: DictionaryDataService;
    let cardDataService: CardDataService;

    beforeAll(() => {
        return TestBed
            .configureTestingModule({imports: [AppModule]})
            .overrideComponent(AppComponent, {set: {template: ""}})
            .overrideComponent(ConfigComponent, {set: {template: ""}})
            .overrideComponent(CardComponent, {set: {template: ""}})
            .overrideComponent(DictionaryComponent, {set: {template: ""}})
            .overrideComponent(PracticeComponent, {set: {template: ""}})
            .compileComponents();
    });

    beforeAll(inject([CardDataService, DictionaryDataService], (card, dictionary) => {
        cardDataService = card;
        dictionaryDataService = dictionary;
    }));

    it('должен очистится перед тестами', (done) => {
        dictionaryDataService
            .clear()
            .then(() => {
                done();
            })
            .catch(() => {
                done.fail("failed");
            });
    });

    it('должен получить пустой список', (done) => {
        dictionaryDataService
            .save(<Dictionary>{name: "Картотека №1"})
            .then(() => {
                cardDataService
                    .findAll(<Dictionary>{id: 1})
                    .then((data: Card[]) => {
                        expect(data.length).toBe(0);
                        done();
                    })
            });
    });

    it('должен получить ошибку при поиске случайной записи', (done) => {
        cardDataService
            .findOne(<Dictionary>{id: 1})
            .then((card: Card) => {
                expect(card).toBeUndefined();
                done()
            })
            .catch(() => {
                done();
            })
    });


    it('должен создать новую запись', (done) => {
        cardDataService
            .save(<Dictionary>{id: 1}, <Card>{word: "яблоко", translate: "an apple"})
            .then(() => {
                cardDataService
                    .findAll(<Dictionary>{id: 1})
                    .then((data: Card[]) => {
                        expect(data.length).toBe(1);
                        done();
                    })
            })
    });

    it('должен создать еще 4 записи', (done) => {
        cardDataService
            .saveList(<Dictionary>{id: 1}, <Card[]> [
                {word: "женщина", translate: "a woman"},
                {word: "девочка", translate: "a girl"},
                {word: "мужчина", translate: "a man"},
                {word: "мальчик", translate: "a boy"}
            ])
            .then(() => {
                cardDataService
                    .findAll(<Dictionary>{id: 1})
                    .then((data: Card[]) => {
                        expect(data.length).toBe(5);
                        done();
                    })
            })
    });

    it('должен получить случайную запись', (done) => {
        cardDataService
            .findOne(<Dictionary>{id: 1})
            .then((card: Card) => {
                expect(card).toBeTruthy();
                done();
            })
    });

    it('должен изменить существующую запись', (done) => {
        cardDataService
            .save(<Dictionary>{id: 1}, <Card>{id: 1, word: "русский", translate: "english"})
            .then(() => {
                cardDataService
                    .findAll(<Dictionary>{id: 1})
                    .then((data: Card[]) => {
                        expect(data.length).toBe(5);
                        expect(data.some((data) => data.id == 1 && data.word == "русский" && data.translate == "english")).toBeTruthy();
                        done();
                    })
            });
    });

    it('должен удалить существующую запись', (done) => {
        cardDataService
            .remove(<Dictionary>{id: 1}, <Card>{id: 1})
            .then(() => {
                cardDataService
                    .findAll(<Dictionary>{id: 1})
                    .then((data: Card[]) => {
                        expect(data.length).toBe(4);
                        done();
                    })
            })
    });

})
;