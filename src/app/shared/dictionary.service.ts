/**
 * Created by vya on 04.01.2017.
 */
import {Injectable} from "@angular/core";
import {Storage} from "@ionic/storage";
import {Dictionary} from "./dictionary.model";

/**
 * Сервис картотеки
 */
@Injectable()
export class DictionaryDataService {

    /**
     * Конструктор сервиса
     * @param storage
     */
    constructor(private storage: Storage) {
    }

    /**
     * Получение всех картотеки
     * @returns {Promise<T>|Promise}
     */
    findAll(): Promise<Dictionary[]> {
        return new Promise(resolve =>
            this.storage
                .get("dictionary")
                .then(JSON.parse)
                .then(data => resolve(data || []))
                .catch(reject => resolve([]))
        )
    }

    /**
     * Удаление картотеки, а также связанных с ней карточек
     * @param dictionary Картотека
     * @returns {Promise<T>|Promise}
     */
    remove(dictionary: Dictionary): Promise<any> {
        return new Promise(resolve => {
            this.findAll()
                .then(list => {
                    Promise
                        .all([
                            this.storage.set("dictionary", JSON.stringify(list.filter(item => dictionary.id != item.id))),
                            this.storage.remove("card_" + dictionary.id)
                        ])
                        .then(() => resolve("Ok"));
                })
        });
    }

    /**
     * Сохранение картотеки
     * @param dictionary Картотека
     * @returns {Promise<T>|Promise}
     */
    save(dictionary: Dictionary): Promise<any> {
        return this.saveList([dictionary]);
    }

    /**
     * Слияние двух списков
     * @param storage_list Список из хранилища
     * @param dictionary_list Сохраняемый список
     * @returns {Dictionary[]} Результирующий список
     */
    private merge(storage_list: Dictionary[], dictionary_list: Dictionary[]): Dictionary[] {
        let list: Dictionary[] = storage_list.splice(0);
        let id: number = list.length ? Math.max.apply(Math, list.map(item => item.id)) : 0;
        dictionary_list.forEach(data => {
            if (data.id) {
                list = list.map(temp => (data.id === temp.id) ? data : temp);
            } else {
                data.id = ++id;
                list.push(data)
            }
        });
        return list;
    }

    /**
     * Сохранение списка картотек
     * @param dictionary_list Список картотек
     * @returns {Promise<T>|Promise}
     */
    saveList(dictionary_list: Dictionary[]): Promise<any> {
        return new Promise(resolve => {
            this.findAll()
                .then(list => {
                    this.storage
                        .set("dictionary", JSON.stringify(this.merge(list, dictionary_list)))
                        .then(() => resolve("Ok"))
                })
        })
    }

    /**
     * Очистка картотеки
     * @returns {Promise<T>|Promise}
     */
    clear(): Promise<any> {
        return new Promise(resolve => this.storage.clear().then(() => resolve("Ok")));
    }
}