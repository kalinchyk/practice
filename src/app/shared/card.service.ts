/**
 * Created by vya on 05.01.2017.
 */
import {Injectable} from "@angular/core";
import {Storage} from "@ionic/storage";
import {Card} from "./card.model";
import {Dictionary} from "./dictionary.model";

@Injectable()
export class CardDataService {

    constructor(private storage: Storage) {
    }

    /**
     * Получение списка карточек из картотеки
     * @param dictionary Картотека
     * @returns {Promise<T>|Promise}
     */
    findAll(dictionary: Dictionary): Promise<Card[]> {
        return new Promise(resolve =>
            this.storage
                .get("card_" + dictionary.id)
                .then(data => resolve(JSON.parse(data) || []))
                .catch(reject => resolve([]))
        )
    }

    /**
     * Извлечение случайной карточки из картотеки
     * @param dictionary
     * @returns {Promise<T>|Promise}
     */
    findOne(dictionary: Dictionary): Promise<Card> {
        return new Promise((resolve, reject) => {
            this
                .findAll(dictionary)
                .then(list => {
                    list.length == 0 ? reject("empty") : resolve(list[Math.floor(Math.random() * list.length)])
                })
        })
    }

    /**
     * Слияние двух списков
     * @param storage_list Список из хранилища
     * @param card_list Сохраняемый список
     * @returns {Card[]} Результирующий список
     */
    private merge(storage_list: Card[], card_list: Card[]): Card[] {
        let list: Card[] = storage_list.splice(0);
        let id: number = list.length ? Math.max.apply(Math, list.map(item => item.id)) : 0;
        card_list.forEach(data => {
            if (data.id) {
                list = list.map(temp => (data.id === temp.id) ? data : temp);
            } else {
                data.id = ++id;
                list.push(data)
            }
        });
        return list;
    }

    /**
     * Сохранение карточки в картотеку
     * @param dictionary Картотека
     * @param card Карточка
     * @returns {Promise<T>|Promise}
     */
    save(dictionary: Dictionary, card: Card): Promise<any> {
        return this.saveList(dictionary, [card]);
    }

    /**
     * Сохранение списка карточек в картотеку
     * @param dictionary Картотека
     * @param card_list Список карточек
     * @returns {Promise<T>|Promise}
     */
    saveList(dictionary: Dictionary, card_list: Card[]): Promise<any> {
        return new Promise(resolve => {
            this.findAll(dictionary)
                .then(list => {
                    this.storage
                        .set("card_" + dictionary.id, JSON.stringify(this.merge(list, card_list)))
                        .then(() => resolve("Ok"))
                })
        })
    }

    /**
     * Удаление карточки из картотеки
     * @param dictionary Картотека
     * @param card Карточка
     * @returns {Promise<T>|Promise}
     */
    remove(dictionary: Dictionary, card: Card): Promise<any> {
        return new Promise(resolve => {
            this
                .findAll(dictionary)
                .then(list => {
                    this
                        .storage
                        .set("card_" + dictionary.id,
                            JSON.stringify(list.filter(item => card.id != item.id)))
                        .then(() => resolve("ok"))
                })
        });
    }

}