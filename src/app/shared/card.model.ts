/**
 * Created by vya on 05.01.2017.
 */
export class Card {
    id: number;
    word: string;
    translate: string;
    rate: number;
}