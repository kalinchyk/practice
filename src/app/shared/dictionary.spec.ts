import {DictionaryDataService} from "./dictionary.service";
import {inject, TestBed} from "@angular/core/testing";
import {Dictionary} from "./dictionary.model";
import {AppModule} from "../app.module";
import {PracticeComponent} from "../practice/practice.component";
import {DictionaryComponent} from "../dictionary/dictionary.component";
import {CardComponent} from "../card/card.component";
import {AppComponent} from "../app.component";
import {ConfigComponent} from "../config/config.component";

describe('Сервис картотеки', () => {
    let dictionaryDataService: DictionaryDataService;

    beforeAll(() => {
        return TestBed
            .configureTestingModule({imports: [AppModule]})
            .overrideComponent(AppComponent, {set: {template: ""}})
            .overrideComponent(ConfigComponent, {set: {template: ""}})
            .overrideComponent(CardComponent, {set: {template: ""}})
            .overrideComponent(DictionaryComponent, {set: {template: ""}})
            .overrideComponent(PracticeComponent, {set: {template: ""}})
            .compileComponents();
    });

    beforeAll(inject([DictionaryDataService], (object) => {
        dictionaryDataService = object;
    }));

    it('должен очистится перед тестами', (done) => {
        dictionaryDataService
            .clear()
            .then(() => {
                done();
            })
            .catch(() => {
                done.fail("failed");
            });
    });

    it('должен получить пустой список', (done) => {
        dictionaryDataService
            .findAll()
            .then((data: Dictionary[]) => {
                expect(data.length).toBe(0);
                done();
            })
    });

    it('должен создать новую запись', (done) => {
        dictionaryDataService
            .save(<Dictionary>{name: "Картотека №1"})
            .then(() => {
                dictionaryDataService
                    .findAll()
                    .then((data: Dictionary[]) => {
                        expect(data.length).toBe(1);
                        done();
                    })
            })
    });

    it('должен создать еще 4 записи', (done) => {
        dictionaryDataService
            .saveList(<Dictionary[]> [
                {name: "Картотека №2"},
                {name: "Картотека №3"},
                {name: "Картотека №4"},
                {name: "Картотека №5"}
            ])
            .then(() => {
                dictionaryDataService
                    .findAll()
                    .then((data: Dictionary[]) => {
                        expect(data.length).toBe(5);
                        done();
                    })
            })
    });

    it('должен изменить существующую запись', (done) => {
        dictionaryDataService
            .save({id: 5, name: "новое название"})
            .then(() => {
                dictionaryDataService
                    .findAll()
                    .then((data: Dictionary[]) => {
                        expect(data.length).toBe(5);
                        expect(data.some((data) => data.id == 5 && data.name == "новое название")).toBeTruthy();
                        done();
                    })
            });
    });

    it('должен удалить существующую запись', (done) => {
        dictionaryDataService
            .remove(<Dictionary>{id: 5})
            .then(() => {
                dictionaryDataService
                    .findAll()
                    .then((data: Dictionary[]) => {
                        expect(data.length).toBe(4);
                        done();
                    })
            })
    });

})
;