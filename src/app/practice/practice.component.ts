/**
 * Created by vya on 05.01.2017.
 */
import {Component} from "@angular/core";
import {Card} from "../shared/card.model";
import {CardDataService} from "../shared/card.service";
import {Events, NavParams, LoadingController, ToastController} from "ionic-angular";
import {CardComponent} from "../card/card.component";
import {Dictionary} from "../shared/dictionary.model";

@Component({
    templateUrl: "practice.component.html"
})
export class PracticeComponent {
    disabled: boolean = true;
    cardComponent: any = CardComponent;
    card: Card;
    translate: string;
    dictionary: Dictionary;

    constructor(private cardDataService: CardDataService,
                private events: Events,
                private navParams: NavParams,
                private loadingController: LoadingController,
                private toastController: ToastController) {

        this.dictionary = this.navParams.data;
        this.events.unsubscribe("practice:random");
        this.events.subscribe("practice:random", () => {
            let load = this.loadingController.create({
                content: "Выбираем фразу"
            });
            load.present();
            this.cardDataService
                .findOne(this.dictionary)
                .then(data => {
                    this.card = data;
                    this.translate = "";
                    load.dismissAll();
                    this.disabled = false;
                })
                .catch(data => load.dismissAll())
        });
    }

    ionViewWillEnter(): void {
        this.events.publish("practice:random", {});
    }

    validateCard(): void {
        if (!this.disabled) {
            this.disabled = true;
            let result: boolean = this.card.translate.toLowerCase().trim() === this.translate.toLowerCase().trim();

            let toast = this.toastController.create({
                message: result ? `Молодец` : `Правильно так:\n${this.card.translate}`,
                cssClass: result ? `toast-success` : `toast-failed`,
                duration: result ? 1500 : 10000,
                position: `top`,
                showCloseButton: true,
                closeButtonText: `Дальше`
            });
            toast.onWillDismiss(() => {
                this.events.publish("practice:random", {});
            });
            toast.present();
        }
    }

}