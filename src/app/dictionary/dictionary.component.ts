import {Component} from "@angular/core";
import {AlertController, ItemSliding, Events} from "ionic-angular";
import {DictionaryDataService} from "../shared/dictionary.service";
import {Dictionary} from "../shared/dictionary.model";
import {CardComponent} from "../card/card.component";
import {PracticeComponent} from "../practice/practice.component";

@Component({
    templateUrl: 'dictionary.component.html'
})
export class DictionaryComponent {
    dictionaryList: Dictionary[] = [];
    cardComponent: any = CardComponent;
    practiceComponent: any = PracticeComponent;

    constructor(private alertController: AlertController,
                private dictionaryDataService: DictionaryDataService,
                private events: Events) {

        events.unsubscribe("dictionary:updated");
        events.subscribe("dictionary:updated", () => {
            this.dictionaryDataService
                .findAll()
                .then(data => {
                    this.dictionaryList = data
                })
        });
        events.publish("dictionary:updated", {});
    }

    newDictionary() {

        this.alertController.create(
            {
                title: "Создание",
                message: "Для создания картотеки необходимо заполнить:",
                inputs: [{
                    name: "name",
                    placeholder: "Наименование"
                }],
                buttons: [{
                    text: "Отмена",
                    role: "cancel"
                }, {
                    text: "Создать",
                    handler: data => {
                        this.dictionaryDataService
                            .save(data)
                            .then(() => {
                                this.events.publish("dictionary:updated", {})
                            })
                    }
                }]
            }
        ).present();
    }

    remDictionary(dictionary: Dictionary, slidingItem: ItemSliding): void {

        this.alertController.create({
            title: "Удаление",
            message: 'Вы действительно хотите удалить картотеку "' + dictionary.name + '"?',
            buttons: [{
                text: "Отмена",
                role: "cancel"
            }, {
                text: "Удалить",
                handler: data => {
                    this.dictionaryDataService
                        .remove(dictionary)
                        .then(() => {
                            this.events.publish("dictionary:updated", {})
                        })
                }
            }]
        }).present();
        slidingItem.close();
    }
}
