import {Component} from "@angular/core";
import {NavParams, Events, AlertController, ItemSliding} from "ionic-angular";
import {CardDataService} from "../shared/card.service";
import {Dictionary} from "../shared/dictionary.model";
import {Card} from "../shared/card.model";

@Component({
    templateUrl: 'card.component.html'
})
export class CardComponent {
    dictionary: Dictionary;
    cardList: Card[] = [];
    isEmpty: boolean;

    constructor(private navParams: NavParams,
                private events: Events,
                private alertController: AlertController,
                private cardDataService: CardDataService) {

        this.dictionary = this.navParams.data[0];
        this.isEmpty = this.navParams.data[1];
        events.unsubscribe("card:updated");
        events.subscribe("card:updated", () => {
            this.cardDataService
                .findAll(this.dictionary)
                .then(data => {
                    this.cardList = data
                })
        });
        events.publish("card:updated", {})
    }

    ionViewDidEnter(): void {
        if (this.isEmpty) this.newCard()
    }

    newCard() {

        let alert = this.alertController.create(
            {
                title: "Создание",
                message: "Для создания карточки необходимо заполнить:",
                inputs: [{
                    name: "word",
                    placeholder: "Русский"
                }, {
                    name: "translate",
                    placeholder: "English"
                }],
                buttons: [{
                    text: "Отмена",
                    role: "cancel"
                }, {
                    text: "Создать",
                    handler: data => {
                        console.log(alert);
                        if (!data.word || !data.translate)
                            return false;

                        let navTransition = alert.dismiss();
                        this
                            .cardDataService
                            .save(this.dictionary, data)
                            .then(() => {
                                navTransition.then(() => {
                                    this.events.publish("card:updated", {});
                                });
                            });
                        return false;
                    }
                }]
            }
        );
        alert.present();
    }

    remCard(card: Card, itemSliding: ItemSliding) {

        this.alertController.create({
            title: "Удаление",
            message: 'Вы действительно хотите удалить карточку "' + card.word + '"?',
            buttons: [{
                text: "Отмена",
                role: "cancel"
            }, {
                text: "Удалить",
                handler: data => {
                    this.cardDataService
                        .remove(this.dictionary, card)
                        .then(() => {
                            this.events.publish("card:updated", {})
                        })
                }
            }]
        }).present();
        itemSliding.close();

    }
}
