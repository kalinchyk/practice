import {NgModule, ErrorHandler} from "@angular/core";
import {IonicApp, IonicModule, IonicErrorHandler} from "ionic-angular";
import {Storage} from "@ionic/storage";
import {AppComponent} from "./app.component";
import {DictionaryComponent} from "./dictionary/dictionary.component";
import {CardComponent} from "./card/card.component";
import {CardDataService} from "./shared/card.service";
import {DictionaryDataService} from "./shared/dictionary.service";
import {PracticeComponent} from "./practice/practice.component";
import {ConfigComponent} from "./config/config.component";

export function provideStorage() {
    return new Storage(['sqlite', 'websql', 'indexeddb'], {name: '__db', storeName: '__practice'});
}

@NgModule({
    declarations: [
        AppComponent,
        ConfigComponent,
        PracticeComponent,
        DictionaryComponent,
        CardComponent
    ],
    imports: [
        IonicModule.forRoot(AppComponent)
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        AppComponent,
        ConfigComponent,
        PracticeComponent,
        DictionaryComponent,
        CardComponent
    ],
    providers: [
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        {provide: Storage, useFactory: provideStorage},
        CardDataService, DictionaryDataService
    ]
})

export class AppModule {
}
