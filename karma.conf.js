module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', 'karma-typescript'],

    files: [
      'src/base.spec.ts',
      'src/**/*.ts',
      'src/**/*.html'
    ],

    exclude: [
      'src/app/main.ts'
    ],

    preprocessors: {
      'src/**/*.ts': ['karma-typescript']
    },
    karmaTypescriptConfig: {
      tsconfig: "./tsconfig.json"
    },
    htmlReporter: {
      outputFile: 'report/index.html'
    },
    reporters: ['progress', 'dots', 'karma-typescript', 'html'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_WARN,
    autoWatch: true,
    browsers: ['PhantomJS'],
    singleRun: false,
    concurrency: Infinity
  })
}