# Инструкция 

- Установить [git](https://git-scm.com/)
- Установить [nodejs](https://nodejs.org)
- В командной строке выполнить:

```
$ npm install -g ionic cordova
```

- Забрать с gitlab проект

```
$ git clone https://gitlab.com/kalinchyk/practice.git
```

- Перейти в проект и выполнить:

```
$ npm install
```

- Запустить в браузере:

```
$ ionic serve -c -s
```